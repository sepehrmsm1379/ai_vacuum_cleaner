package com.sepehr.object;

import com.sepehr.env.Environment;
import com.sepehr.env.Place;

public class VacuumCleaner extends Thread implements Runnable {

    private final Environment environment;

    private int xPos;
    private int yPos;


    public VacuumCleaner(int xPos, int yPos, Environment environment) {
        this.xPos = xPos;
        this.yPos = yPos;

        this.environment = environment;
    }

    public void right(){
        if (xPos + 1 < environment.getWidth()){
            ++xPos;
        }
    }

    public void left(){
        if (xPos - 1 >= 0) {
            --xPos;
        }
    }

    public void up(){
        if (yPos + 1 < environment.getHeight()) {
            ++yPos;
        }
    }

    public void down(){
        if (yPos - 1 >= 0 ) {
            --yPos;
        }
    }

    /**
     * @return The nearest dirty place
     */
    public Place getNearestDirtyPlace(){
       int distance = Integer.MAX_VALUE;
       Place[][] places = environment.getPlaces();

       Place place = new Place(0, 0);
       for (int x = 0; x < environment.getWidth(); x++){
           for (int y = 0 ; y < environment.getHeight(); y++){
               int newDistance = Math.abs(this.xPos - x) + Math.abs(this.yPos - y);
               if (newDistance < distance && places[y][x].isDirty()){
                   distance = newDistance;
                   place = places[y][x];
               }
           }
       }
       return place;
    }

    public void clean(){
        // While current place is dirty clean it!
        while (environment.getPlaces()[yPos][xPos].isDirty())
            environment.getPlaces()[yPos][xPos].decreaseDirtyLevel();
    }

    public void move(){
        // first if current position is dirty clean it
        clean();

        // Find most dirty place
        Place mostDirtyPlace = environment.getMostDirtyPlace();
        // Find the nearest dirty place
        Place nearestDirtyPlace = getNearestDirtyPlace();

        // Select next place
        Place place = mostDirtyPlace.getDirtyLevel() > nearestDirtyPlace.getDirtyLevel() ? mostDirtyPlace: nearestDirtyPlace;

        // Move towards dirty place
        if (place.getxPos() > xPos )
            right();
        else if (place.getxPos() < xPos)
            left();
        else if (place.getyPos() > yPos)
            up();
        else if (place.getyPos() < yPos)
            down();
    }

    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }

    @Override
    public void run(){
        while (true){
            move();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
