package com.sepehr.env;

/**
 * Every place is clean or dirty
 */
public  class Place {

    // Initial value is 0
    private int dirtyLevel;

    private final int xPos;
    private final int yPos;

    public Place(int yPos, int xPos) {
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public void increaseDirtyLevel(){
        ++dirtyLevel;
    }

    public void decreaseDirtyLevel(){
        if (dirtyLevel - 1 < 0)
            throw new IllegalArgumentException("Dirty level should be greater than zero");
        --dirtyLevel;
    }

    public boolean isDirty(){
        return dirtyLevel > 0;
    }

    public int getDirtyLevel() {
        return dirtyLevel;
    }

    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }

    @Override
    public String toString() {
        return dirtyLevel < 1 ? " " : String.valueOf(dirtyLevel);
    }


}
