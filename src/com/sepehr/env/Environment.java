package com.sepehr.env;

import com.sepehr.object.VacuumCleaner;

import java.util.Random;
import java.util.stream.IntStream;

public class Environment extends Thread implements Runnable {

    protected final Place[][] places;

    private final int width;
    private final int height;

    private final VacuumCleaner vacuumCleaner;

    private static final Random random = new Random();

    /**
     * Create environment with custom size
     */
    public Environment(int width, int height) {
        if (width < 1 || height < 1)
            throw new IllegalArgumentException("Args should greater than zero");

        this.width = width;
        this.height = height;
        this.places = new Place[height][width];
        IntStream.range(0, width).forEach(x -> IntStream.range(0, height).forEach(y ->
                this.places[y][x] = new Place(y, x)));


        // Random position for vacuum
        vacuumCleaner = new VacuumCleaner(random.nextInt(width), random.nextInt(height), this);
    }

    public int getDirtyLevel(){
        int counter = 0;
        for (int x = 0; x < height ; x++){
            for (int y = 0 ; y <width ; y++)
                counter += places[x][y].getDirtyLevel();
        }
        return counter;
    }

    public Place getMostDirtyPlace(){
        Place place = new Place(0, 0);
        for (int x = 0; x < width ; x++){
            for (int y = 0; y < height ; y++){
                if (places[x][y].getDirtyLevel() > place.getDirtyLevel())
                    place = places[x][y];
            }
        }
        return place;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Place[][] getPlaces() {
        return places;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("");
        for (int y = 0 ; y < height; y++){
            for (int x = 0; x < width ; x++){
                if (x == vacuumCleaner.getxPos() && y == vacuumCleaner.getyPos())
                    stringBuilder.append("&");
                else
                    stringBuilder.append(places[y][x]);
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public void run(){
        // Every 5 seconds dirty level increased
        vacuumCleaner.start();
        while (true){
            places[random.nextInt(height)][random.nextInt(width)].increaseDirtyLevel();
            System.out.println(this + "\n\n\n\n\n");
            try {
                Thread.sleep(950);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
