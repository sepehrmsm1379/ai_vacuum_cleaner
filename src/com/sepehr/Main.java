package com.sepehr;

import com.sepehr.env.Environment;

public class Main {

    public static void main(String[] args) {

        new Environment(10, 10).start();

    }
}
